import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { ShopComponent} from "./shop/shop.component";
import { LoginPageComponent } from "./login-page/login-page.component";
import { RegistrationFormComponent } from "./registration-form/registration-form.component";
import { FirebaseComponent } from "./firebase/firebase.component";

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
  },
  {
    path: 'shop',
    component: ShopComponent
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'register',
    component: RegistrationFormComponent,
  },
  {
    path: 'firebase',
    component: FirebaseComponent,
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
