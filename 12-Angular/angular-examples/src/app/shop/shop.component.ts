import { Component, OnInit } from '@angular/core';
import { ShopService } from "../shop.service";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  shopdata: Array<string[]>;
  shopItem: string[];

  constructor(private shopService: ShopService) {
    this.shopItem = ['','',''];
  }

  ngOnInit() {
    this.shopService.getShopData().subscribe(
      data => {
        this.shopdata = data;
      },
      err => { console.log(err)},
      () => { console.log('done')},
    );
  }

  saveProduct() {
    this.shopService.newProduct(this.shopItem).subscribe(
      (data: any) => {
        console.log(data);
        this.shopdata = data;
      },
      err => {
        console.log(err);
      }
    );
  }
}
