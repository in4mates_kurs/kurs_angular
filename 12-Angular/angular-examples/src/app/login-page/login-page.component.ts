import { Component, OnInit } from '@angular/core';
import { LoginData } from "./LoginData";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginData: LoginData;

  constructor() {
    this.loginData = new LoginData();
  }

  ngOnInit() {

  }
  newForm() {
    this.loginData = new LoginData();
  }
  onSubmit() {
    // do something
  }

}
