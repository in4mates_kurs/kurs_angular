import { Injectable } from '@angular/core';

export interface Customer {
  name: string;
  lastname: string;
}

@Injectable({
  providedIn: 'root'
})
export class CustomersListService {

  private _customers_: Customer[];

  get customers(): Customer[] {
    return this._customers_;
  }

  constructor() {
    this._customers_ = [
      { name: 'John', lastname: 'Smith' },
      { name: 'Richard', lastname: 'Chamberline' },
      { name: 'Jan', lastname: 'Kowalski' },
    ];
  }
  push(customer: Customer) {
    this._customers_.push(customer);
  }
}
