import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/index";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ShopService {

  constructor(private http: HttpClient) {}

  getShopData():Observable<any> {
    return this.http.get('http://localhost:3000/shop-data');
  }

  newProduct(data: string[]) {
    let body = JSON.stringify(data);
    return this.http.post('http://localhost:3000/new', { data }, httpOptions);
  }
}
