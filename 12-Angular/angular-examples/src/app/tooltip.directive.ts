import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements AfterViewInit{
  element: HTMLElement;
  @Input() appTooltip: string;
  constructor(private elementRef: ElementRef) {}
  ngAfterViewInit() {
    this.element = this.elementRef.nativeElement;
    this.element.parentNode.insertBefore(this.tooltipElement(), this.element);
  }
  tooltipElement(): HTMLElement {
    const el: HTMLElement = document.createElement('div');
    el.innerText = this.appTooltip;
    return el;
  }
}
