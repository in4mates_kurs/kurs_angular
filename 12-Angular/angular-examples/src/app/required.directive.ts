import {AfterViewInit, Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appRequired]'
})
export class RequiredDirective implements AfterViewInit {
  element: HTMLInputElement;
  label: HTMLLabelElement;
  @Input() appRequired: boolean;

  constructor(ref: ElementRef) {
    this.element = ref.nativeElement;
  }
  ngAfterViewInit() {
    this.element.required = this.appRequired;
    const parent = this.element.parentElement;
    parent.querySelector('label').appendChild(this.astrix());
  }

  private astrix(): HTMLSpanElement {
    const astrix: HTMLSpanElement = document.createElement('span');
    astrix.style.color = 'red';
    astrix.innerText = ' *';
    return astrix;
  }
  @HostListener('mouseenter') onMouseEnter() {
    this.element.style.borderColor = 'red';
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.element.style.borderColor = '#202020';
  }
}
