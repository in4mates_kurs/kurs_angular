import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { RequiredDirective } from './required.directive';
import { TooltipDirective } from './tooltip.directive';
import { CustomersListService } from "./customers-list.service";
import { ShopComponent } from './shop/shop.component';
import { ShopService } from "./shop.service";
import { LoginPageComponent } from './login-page/login-page.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { FirebaseComponent } from './firebase/firebase.component';
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireDatabaseModule } from "angularfire2/database";

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    InputFieldComponent,
    RequiredDirective,
    TooltipDirective,
    ShopComponent,
    LoginPageComponent,
    RegistrationFormComponent,
    FirebaseComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule
  ],
  providers: [CustomersListService, ShopService],
  bootstrap: [AppComponent]
})
export class AppModule { }
