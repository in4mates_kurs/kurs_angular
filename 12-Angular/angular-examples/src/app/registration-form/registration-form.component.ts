import { Component, Input, OnChanges }       from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})

export class RegistrationFormComponent {

  regForm: FormGroup;
  someForm: FormGroup;

  constructor(private fb: FormBuilder) {

    this.createForm();
    this.someForm = new FormGroup({
      name: new FormControl(),
      lastname: new FormControl(),
    })
  }

  createForm() {
    this.regForm = this.fb.group({
      firstname: ['', Validators.required],
      addresses: this.fb.array([this.createItem()]),
      username: ['', Validators.required, Validators.minLength(10)],
      lastname: ['', Validators.required]
    });
  }

  createItem(): FormGroup {
    return this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required]
    });
  }

  addAddress() {
    const addresses = this.regForm.get('addresses') as FormArray;
    addresses.push(this.createItem());
  }

  getArrayItem(i: number, name: string): FormControl {
    const formArray: FormArray = <FormArray>this.regForm.get('addresses');
    return <FormControl>(<FormGroup>formArray.controls[i]).get(name);
  }

  hidden(name: string, i?: number) {
    const control: FormControl = (i === undefined) ? <FormControl>this.regForm.get(name) : this.getArrayItem(i, name);
    return control.valid || control.pristine;
  }
}
