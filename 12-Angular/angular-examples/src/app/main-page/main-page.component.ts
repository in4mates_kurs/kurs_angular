import { Component, OnInit } from '@angular/core';
import {Customer, CustomersListService} from "../customers-list.service";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {

  customers: Customer[];
  customer: Customer;

  constructor(private customersService: CustomersListService) {
    this.customer = {
      name: '',
      lastname: '',
    };
  }

  ngOnInit() {
    this.customers = this.customersService.customers;
  }

  pushCustomer() {
    this.customersService.push(this.customer);
    this.customer = {
      name: '',
      lastname: '',
    };
  }
}
