import { Component } from '@angular/core';
import { Observable } from "rxjs/index";
import {AngularFireDatabase, AngularFireObject, AngularFireList} from "angularfire2/database";

@Component({
  selector: 'app-firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.scss']
})

export class FirebaseComponent {
  items: Observable<any[]>;
  newItem: string = '';
  itemsRef: AngularFireList<any>;
  constructor(db: AngularFireDatabase) {
    this.itemsRef = db.list('items');
    this.items = this.itemsRef.valueChanges();
  }

  pushToDb() {
    this.itemsRef.push(this.newItem);
  }
}
