const express = require('express');
const data = require('./data');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get('/shop-data', (req, res) => res.send(data.products));

app.post('/new', (req, res) => {
    console.log(typeof req.body);
    data.products.push(req.body.data);
    res.send(data.products);
});
app.post('/buy', (req, res) => {
    const index = data.products.findIndex((item) => item[0] === req.body.data[0]);
    if(index !== -1) {
        const item = data.products[index];
        const count = parseInt(item[2]);
        const buyCount = parseInt(req.body.data[2]);
        data.products[index][2] = (count - buyCount).toString();
        res.send(data.products);
    } else {
        res.code(500);
    }
});
app.post('/remove', (req, res) => {
    const index = data.products.findIndex((item) => item[0] === req.body.data[0]);
    if(index !== -1) {
        data.products.splice(index, 1);
        res.send(data.products);
    } else {
        res.code(500);
    }
});
app.listen(3000, () => console.log('Example app listening on port 3000!'));