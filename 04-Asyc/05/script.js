var URL = 'http://localhost:3000';

var RestClient = function () {
    this.ajax = new XMLHttpRequest();
};
RestClient.DONE = 4;
RestClient.OK = 200;
RestClient.prototype.stateChangeHandler = function() {
    if (this.ajax.readyState === RestClient.DONE) {
        if (this.ajax.status === RestClient.OK) {
            try {
                const data = JSON.parse(this.ajax.responseText);
                this.resolve(data);
            } catch (err) {
                this.reject(err);
            }
        } else {
            this.reject('Error: ' + this.ajax.status);
        }
    }
};
RestClient.prototype.get = function(path) {
    return new Promise(function(resolve, reject) {
        this.resolve = resolve;
        this.reject = reject;
        this.ajax.open('GET', URL + path, true);
        this.ajax.onreadystatechange = this.stateChangeHandler.bind(this);
        this.ajax.send();
    }.bind(this));
};
RestClient.prototype.post = function(path, data) {
    return new Promise(function(resolve, reject) {
        this.resolve = resolve;
        this.reject = reject;
        this.ajax.open('POST', URL + path, true);
        this.ajax.onreadystatechange = this.stateChangeHandler.bind(this);
        this.ajax.setRequestHeader("Content-Type", "application/json");
        this.ajax.send(JSON.stringify({data}));
    }.bind(this));
};

var TableData = function (tableSelector) {
    this.table = document.querySelector(tableSelector);
    this.ajax = new RestClient();
    this.table.className = 'data-table';
    var head = this.tableRow('th', ['Lp', 'Nazwa', 'Cena', 'Ilość', 'Narzędzia']);
    this.table.appendChild(head);

    this.ajax.get('/shop-data').then(function(response){
        response.forEach(function (data) {
            this.addRow(data);
        }.bind(this));
    }.bind(this));
};

TableData.prototype.tableRow = function (cell, cellData) {
    var tr = document.createElement('tr');
    var count = (typeof cellData === 'object') ? cellData.length : cellData;
    for (var i = 0; i < count; i++) {
        var td = document.createElement(cell);
        if(typeof cellData === 'object') {
            td.innerText = cellData[i];
        }
        tr.appendChild(td);
    }
    return tr;
};

TableData.prototype.addRow = function(data) {
    var rowCount = this.table.querySelectorAll('tr').length;
    data.unshift(rowCount);
    var rw = this.tableRow('td', data);
    var length = this.table.querySelectorAll('tr').length;
    rw.appendChild(this.toolCell(length));
    this.table.appendChild(rw);
};

TableData.prototype.toolCell = function(idx) {
    var cell = document.createElement('td');
    cell.style.padding = '5px';
    cell.style.textAlign = 'right';

    var buyBttn = this.toolButton('Kup');
    buyBttn.dataset.index = idx;
    buyBttn.addEventListener('click', this.buyClickHandler.bind(this));

    var removeBttn = this.toolButton('Usuń');
    removeBttn.dataset.index = idx;
    removeBttn.addEventListener('click', this.removeClickHandler.bind(this));

    cell.appendChild(buyBttn);
    cell.appendChild(removeBttn);
    return cell;
};

TableData.prototype.toolButton = function(label) {
    var bttn = document.createElement('button');
    bttn.innerText = label;
    bttn.className = 'button tool-button';
    return bttn;
};

TableData.prototype.getRowData = function(idx) {
    var rows = this.table.querySelectorAll('tr');
    var tds = rows[idx].querySelectorAll('td');
    return [tds[1].innerText, parseInt(tds[2].innerText), parseInt(tds[3].innerText)];
};

TableData.prototype.buyClickHandler = function(ev) {
    var idx = parseInt(ev.target.dataset.index, 10);
    var rowData = this.getRowData(idx);
    var message = 'Czy chcesz kupić ' + rowData[0] + ' za ' + rowData[1] + 'zł?';
    this.buyAction(message);
};

TableData.prototype.rebuildIndex = function() {
    var rows = this.table.querySelectorAll('tr');
    for(var i = 1; i < rows.length; i++) {
        var row = rows[i];
        var btns = row.querySelectorAll('.tool-button');
        btns[0].dataset.index = i;
        btns[1].dataset.index = i;
        var idxCell = row.querySelector('td');
        idxCell.innerText = i;
    }
};

TableData.prototype.removeClickHandler = function(ev) {
    var idx = ev.target.dataset.index;
    var row = this.table.querySelectorAll('tr')[idx];
    row.remove();
    this.rebuildIndex();
};

TableData.prototype.setBuyAction = function(buyAction) {
    this.buyAction = buyAction;
};

var Modal = function(selector) {
    this.modal = document.querySelector(selector);
    this.modalBody = this.modal.querySelector('.modal-body');
    var close = this.modal.querySelectorAll('[data-action=close]');
    for(var i = 0; i < close.length; i++) {
        var closeBttn = close[i];
        closeBttn.addEventListener('click', this.close.bind(this));
    }
};

Modal.prototype.show = function (html) {
    this.modalBody.innerHTML = html;
    this.modal.classList.add('show');
};

Modal.prototype.close = function () {
    this.modal.classList.remove('show');
};

(function () {
    var app = document.querySelector('#app');
    var addArticleBttn = document.querySelector('#add-article');
    var nazwa = document.querySelector('#nazwa');
    var cena = document.querySelector('#cena');
    var ilosc = document.querySelector('#ilosc');
    var restClient = new RestClient();

    var modal = new Modal('#confirm-modal');
    var tableData = new TableData('#data-table');
    tableData.setBuyAction(modal.show.bind(modal));

    var postData = function (path, itemData) {
        return restClient.post(path, itemData);
    };

    var clearForm = function () {
        nazwa.value = '';
        cena.value = '';
        ilosc.value = '';
    };

    addArticleBttn.addEventListener('click', function () {
        if(nazwa.value && cena.value && ilosc.value) {
            var data = [nazwa.value, cena.value, ilosc.value];

            postData('/new', data)
                .then(function (response) {
                    tableData.addRow(data);
                    clearForm();
                })
                .catch(function (err) {
                    window.alert(err);
                });
        }
    });
}());