var button = document.querySelector('#button-1');
var input = document.querySelector('#input-1');
var paragraph = document.querySelector('#paragraph_1');

button.addEventListener('click', function(event) {
    window.alert(event.target.textContent);
});

input.addEventListener('change', function(event) {
    window.alert(event.target.value);
});

input.addEventListener('keyup', function(event) {
    paragraph.innerText = event.which;
});
