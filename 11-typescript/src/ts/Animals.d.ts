declare namespace Animals {
    export interface Animal {
        species: string;
        giveVoice(voice: string): void;
    }

    export interface Mammal extends Animal {
        hasFur: boolean;
    }
}