/// <reference path="Animals.d.ts" />


class Mammal implements Animals.Mammal {
    species: string;
    hasFur: boolean;

    constructor(species: string) {
        this.species = species;
        this.hasFur = true;
    }

    giveVoice(voice: string): void {
        console.log(`${this.species} makes ${voice}`);
    }
}