let isDone: boolean = false;
let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;
let color: string = "blue";

let list: number[] = [1, 2, 3];
let list2: Array<number> = [1, 2, 3];
let x: [string, number] = ["hello", 10];
console.log(x[0]);

let myTuple = ['Hello world', 10, true];
let [a , b, c] = myTuple;
console.log(a);

let item: null | HTMLElement;

let z: number | string;
z = 'foo';
z = 123;

const h1: HTMLHeadingElement | null = document.querySelector('h1');
if(h1 !== null) {
    // Do something
}
enum Color {Red, Green, Blue}
enum Cars {Volvo=1, BMW, Audi}
enum Computers {Linux=1, Mac=4, PC=6}

enum Paths {
    HOME = '/home/',
    REGISTRATION = '/auth/registration',
    SIGN_IN = '/auth/sign-in',
}

enum Data {
    ONE =  '123'.length
}

let list3: any[] = [1, true, "free"];

const do_something = function(param: string): void {
    console.log('Hello world ' + param);
};

const plus = function(paramOne: number, paramTwo: number): number {
    return paramOne + paramTwo;
};

const multiply = (paramOne: number, paramTwo: number): number => paramOne * paramTwo;

let param: any = 'Lorem ipsum';
let stringParam: string = <string>param;

console.log((param as string).length);

interface Square {
    width: number;
    area: number;
}

function createSquare (width: number): Square {
    return {
        width,
        area: width * width,
    };
}

var sqr: Square = createSquare(45);
console.log(sqr.area);

interface User {
    name: string;
    lastname: string;
}

type UserCallback = (user: User) => boolean;

function fetchUser(callback: UserCallback) { }

function fetchUserCallback(user:User) {
    if (user.name === 'Michal') {
        return true;
    }
    return false;
}

fetchUser(fetchUserCallback);

interface Role {
    name: string,
    readonly enable: boolean,
}
interface Options {
    [key: string] : any;
}
interface User {
    name: string;
    age: number;
    active: boolean;
    readonly roles: Role[];
    nick?: string;
    options: Options;
}

function extend<T, U>(first: T, second: U): T & U {
    let result = <T & U>{};
    return result;
}

interface Car {
    brand: string;
    maxSpeed: number;
    drive(speed: number): void;
    countWheels(spare: boolean): number;
    accessory: Array<string>
}

interface StringArray {
    [index: number]: string;
    readonly length: number;
    push(item: string): void;
}

interface StringMap {
    [index: string]: string;
}

interface ClockInterface {
    currentTime: Date;
    readonly minutes: number;
    readonly hours: number;
    setTime(d: Date): void;
}

class Clock implements ClockInterface {
    currentTime: Date;

    get hours(): number {
        return this.currentTime.getHours();
    }

    get minutes(): number {
        return this.currentTime.getMinutes();
    }

    constructor() {
        this.currentTime = new Date();
    }
    setTime(d: Date): void {
        this.currentTime = d;
    }
}

abstract class Animal {
    species: string;

    constructor(species: string) {
        this.species = species;
    }

    giveVoice(voice: string) {
        let message = `${this.species} makes ${voice}`;
        console.log(message);
    }
}

class Dog extends Animal {
    breed: string;
    weight: number;

    get size(): string {
        return this.evaluateWeight();
    }
    constructor(breed: string, weight: number) {
        super('Dog');
        this.breed = breed;
        this.weight = weight;
    }
    private evaluateWeight(): string {
        switch (true) {
            case this.weight < 10:
                return 'small';
            case this.weight < 20:
                return 'medium';
            default:
                return 'big';
        }
    }
}

const dog = new Dog('German Shepherd', 40);
console.log(dog.size);
dog.giveVoice('bark');


interface Named {
    name: string;
}
class Person {
    name: string;
}
let p: Named;
// OK, because of structural typing
p = new Person();