require('../scss/style.scss');

import Modal from './Modal';
import TableData from './TableData';

var app = document.querySelector('#app');
var addArticleBttn = document.querySelector('#add-article');
var nazwa = document.querySelector('#nazwa');
var cena = document.querySelector('#cena');
var ilosc = document.querySelector('#ilosc');

var modal = new Modal('#confirm-modal');
var tableData = new TableData('#data-table');

tableData.setBuyAction(modal.show.bind(modal));
addArticleBttn.addEventListener('click', function () {
    if(nazwa.value && cena.value && ilosc.value) {
        var data = [nazwa.value, cena.value, ilosc.value];
        tableData.addRow(data);
        nazwa.value = '';
        cena.value = '';
        ilosc.value = '';
    }
});