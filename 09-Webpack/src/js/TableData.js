export default class TableData {

    constructor(tableSelector) {
        this.table = document.querySelector(tableSelector);
        this.table.className = 'data-table';
        var head = this.tableRow('th', ['Lp', 'Nazwa', 'Cena', 'Ilość', 'Narzędzia']);
        this.table.appendChild(head);
        var initData = [['Masło', '5', '10'], ['Margaryna', '3', '10'], ['Chleb', '2', '10'], ['Bułki', '1', '10']];
        initData.forEach(function (data) {
            this.addRow(data);
        }.bind(this));
    }

    tableRow(cell, cellData) {
        var tr = document.createElement('tr');
        var count = (typeof cellData === 'object') ? cellData.length : cellData;
        for (var i = 0; i < count; i++) {
            var td = document.createElement(cell);
            if(typeof cellData === 'object') {
                td.innerText = cellData[i];
            }
            tr.appendChild(td);
        }
        return tr;
    }

    addRow(data) {
        var rowCount = this.table.querySelectorAll('tr').length;
        data.unshift(rowCount);
        var rw = this.tableRow('td', data);
        var length = this.table.querySelectorAll('tr').length;
        rw.appendChild(this.toolCell(length));
        this.table.appendChild(rw);
    }
    toolCell(idx) {
        var cell = document.createElement('td');
        cell.style.padding = '5px';
        cell.style.textAlign = 'right';

        var buyBttn = this.toolButton('Kup');
        buyBttn.dataset.index = idx;
        buyBttn.addEventListener('click', this.buyClickHandler.bind(this));

        var removeBttn = this.toolButton('Usuń');
        removeBttn.dataset.index = idx;
        removeBttn.addEventListener('click', this.removeClickHandler.bind(this));

        cell.appendChild(buyBttn);
        cell.appendChild(removeBttn);
        return cell;
    }

    toolButton(label) {
        var bttn = document.createElement('button');
        bttn.innerText = label;
        bttn.className = 'button tool-button';
        return bttn;
    }

    getRowData(idx) {
        var rows = this.table.querySelectorAll('tr');
        var tds = rows[idx].querySelectorAll('td');
        return [tds[1].innerText, parseInt(tds[2].innerText), parseInt(tds[3].innerText)];
    }

    buyClickHandler(ev) {
        var idx = parseInt(ev.target.dataset.index, 10);
        var rowData = this.getRowData(idx);
        var message = 'Czy chcesz kupić ' + rowData[0] + ' za ' + rowData[1] + 'zł?';
        this.buyAction(message);
    }

    rebuildIndex() {
        var rows = this.table.querySelectorAll('tr');
        for(var i = 1; i < rows.length; i++) {
            var row = rows[i];
            var btns = row.querySelectorAll('.tool-button');
            btns[0].dataset.index = i;
            btns[1].dataset.index = i;
            var idxCell = row.querySelector('td');
            idxCell.innerText = i;
        }
    }

    removeClickHandler(ev) {
        var idx = ev.target.dataset.index;
        var row = this.table.querySelectorAll('tr')[idx];
        row.remove();
        this.rebuildIndex();
    }

    setBuyAction(buyAction) {
        this.buyAction = buyAction;
    }
}