export default class Modal {
    constructor(selector) {
        this.modal = document.querySelector(selector);
        this.modalBody = this.modal.querySelector('.modal-body');
        var close = this.modal.querySelectorAll('[data-action=close]');
        for(var i = 0; i < close.length; i++) {
            var closeBttn = close[i];
            closeBttn.addEventListener('click', this.close.bind(this));
        }
    }

    show(html) {
        this.modalBody.innerHTML = html;
        this.modal.classList.add('show');
    }

    close() {
        this.modal.classList.remove('show');
    }
}
