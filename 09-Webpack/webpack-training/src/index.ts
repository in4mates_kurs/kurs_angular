declare var require: {
    <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void): void;
    ensure: (
        paths: string[],
        callback: (require: <T>(path: string) => T) => void
    ) => void;
};

require('./style/style.scss');

import Modal from './ts/Modal';

const app = document.querySelector('#app');

// const bttn = document.createElement('button');
// bttn.innerHTML = "Click me, pleeease!!!";
// bttn.dataset.message = 'Hurra, It is alive!';
// app.appendChild(bttn);

const modal: Modal = new Modal();