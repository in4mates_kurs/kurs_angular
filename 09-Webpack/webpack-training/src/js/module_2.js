class Visual {
  constructor(id) {
    this.styles = {
      width: '100vw',
      height: '100vh',
      backgroundColor: 'lightblue',
    };

    this.app = document.querySelector(`#${id}`);
  }
  changeStyle() {
    const keys = Object.keys(this.styles);
    keys.forEach((key) => {
      this.app.style[key] = this.styles[key];
    });
  }
}
export default Visual;
