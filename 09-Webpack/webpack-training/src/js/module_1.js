class Api {
  get apiColor() {
    return this.mainColor;
  }
  set apiColor(value) {
    this.mainColor = value;
  };

  constructor() {
    this.mainColor = 'red';
  }
  _concat(message) {
    return message + ' fooo';
  };

  showConsoleMessage(message) {
    console.log(message);
  };

  showAlertMessage (message) {
    window.alert(this._concat(message));
  };
}

export default Api;