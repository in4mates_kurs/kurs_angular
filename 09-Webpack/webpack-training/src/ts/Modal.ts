export default class Modal {
    modalEl: HTMLElement;
    modalContent: HTMLElement;

    constructor() {
        this.modalEl = document.createElement('div');
        this.modalEl.className = 'modal';
        const close = this.closeBttn();
        close.addEventListener('click', this.closeModal.bind(this));
        this.modalEl.appendChild(close);
        this.modalContent = document.createElement('div');
        this.modalEl.appendChild(this.modalContent);
        document.body.appendChild(this.modalEl);
        this.setHandlers();
    }

    setHandlers() {
        const bttns: NodeListOf<Element> = document.querySelectorAll('[data-message]');
        bttns.forEach((element: Element) => {
            element.addEventListener('click', this.triggerBttnClickHandler.bind(this));
        });
    }

    closeModal(): void {
        this.modalEl.classList.remove('show');
    }

    showMessage(message: string): void {
        this.modalContent.innerHTML = message;
        this.modalEl.classList.add('show');
    }

    triggerBttnClickHandler(event: MouseEvent) {
        const bttn: HTMLElement = event.target as HTMLElement;
        const message: string = bttn.dataset.message;
        this.showMessage(message);
    }

    closeBttn() {
        const close: HTMLElement = document.createElement('div');
        close.innerHTML = '&#215';
        close.className = 'close';
        return close;
    }
}