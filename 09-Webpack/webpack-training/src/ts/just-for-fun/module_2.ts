interface User {
    name: string;
    lastname: string;
    age: number;
    privilages:string[];
    female?: boolean;
    drinkedBeers():number;
}


class TrainingParticipant implements User {
    name: string;
    lastname: string;
    age: number;
    privilages:string[];
    female?: boolean;

    get fullName(): string {
        return `${this.name} ${this.lastname}`;
    }

    set fullName(value: string) {
        const splited: string[] = value.split(' ');
        this.name = splited[0];
        this.lastname = splited[1];
    }

    constructor(name: string) {
        this.name = name;
    }

    drinkedBeers():number {
        return this.age * 100;
    }

    createFemaleParticipat(): void {
        this.female = true;
    }
}


class SuperTrainingParticipat extends TrainingParticipant {
    constructor(name: string) {
        super(name);
    }
    hasScore(): number {
        return this.drinkedBeers() < 1000 ? 1 : 5
    }
    showFullName(): void {
        const lastname = this.fullName;
        window.alert(lastname);
    }
    setFullName(fullName:string) {
        this.fullName = fullName;
    }
}