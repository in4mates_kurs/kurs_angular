class Foo {
    col: Array<string>;
    constructor() {
       this.col = ['a', 'b', 'c', 'b'];
    }
    count():void {
        this.col.forEach((item: string) => {
            console.log(this.concat(item));
        });
    }
    bOnly(): string[] {
        let b: string[] = this.col.filter((item:string) => item === 'b');
        return b;
    }
    concat(item:string):string {
        return `${item} fooo`;
    }
    foo(param: any):string {
        let stringParam: string;
        stringParam = param as string;
        return stringParam;
    }
}
