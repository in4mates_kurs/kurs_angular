var Human = function(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.friends = ['dog', 'cat'];
};
Human.prototype.bodyParts = ['head', 'leg', 'arms'];
Human.prototype.sayHello = function () {
    console.log('Hello ' + this.firstName);
};
var person1 = new Human("John", "Smith");
var person2 = new Human("Jan", "Kowalski");
person1.sayHello = function(when) {
    console.log('Hello ' + this.firstName + ' ' + this.lastName + ' ' + when);
};