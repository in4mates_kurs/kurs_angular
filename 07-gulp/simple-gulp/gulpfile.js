var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var babel = require("gulp-babel");

gulp.task('default', function () {
    gulp.src('src/*.js')
        .pipe(gulp.dest('dist'));
});

gulp.task('concat', function () {
    gulp.src('src/*.js')
        .pipe(concat('concated.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('uglify', function () {
    gulp.src('src/*.js')
        .pipe(concat('concated.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function () {
    return gulp.src('./sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task("js", function () {
    return gulp.src("src/Modal.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task('js:watch', function () {
    gulp.watch('./src/*.js', ['js']);
});